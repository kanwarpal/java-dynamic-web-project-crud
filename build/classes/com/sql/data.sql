use crud;

CREATE TABLE `userInfo` (
  `firstName` varchar(100) NOT NULL default '',
  `lastName` varchar(100) default NULL,
  `middleName` varchar(100) default NULL,
  `address` varchar(100) default NULL,
  `city` varchar(100) default NULL,
   `state` varchar(100) default NULL,
    `country` varchar(100) default NULL,
     `phone` varchar(100) default NULL,
     `bankname` varchar(100) default NULL,
      `account` varchar(100) default NULL,
   `ssn` varchar(100) default NULL,
  PRIMARY KEY  (`firstName`)
) ;

insert into userInfo(`firstName`,`lastName`,`middleName`, `address`, `city`, `state`, `country`, `phone`, `bankname`,`account`,`ssn`) values ('Waris','Khanna','Singh', 'san lorenzo', 'sf', 'cali', 'usa', '1212', 'DENA','13123','000');
