<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;}

h2{
			text-align: center;
			text-transform: uppercase;
			border: 10px solid grey;
			padding: 10px 0;
		}

</style>
</head>
<body>

<jsp:useBean id="beanUsed" class="com.bean.Bean" scope="session"/>	
<h2>You have registered SUCCESSFULLY !!!</h2>
<table id="customers">
<tr>
<td> FirstName</td>
<td><jsp:getProperty property="firstName" name="beanUsed"/></td>
</tr>
<tr>
<td>lastName</td>
<td> <jsp:getProperty property="lastName" name="beanUsed"/></td>
</tr>
<tr>
<td>MiddleName</td>
<td><jsp:getProperty property="middleName" name="beanUsed"/></td>
</tr>
<tr>
<td>Address</td>
<td><jsp:getProperty property="address" name="beanUsed"/></td>
</tr>
<tr>
<td>City</td>
<td><jsp:getProperty property="city" name="beanUsed"/></td>
</tr>
<tr>
<td>State</td>
<td><jsp:getProperty property="state" name="beanUsed"/></td>
</tr>
<tr>
<td>Country</td>
<td><jsp:getProperty property="country" name="beanUsed"/></td>
</tr>
<tr>
<td>Phone</td>
<td> <jsp:getProperty property="phone" name="beanUsed"/></td>
</tr>
<tr>
<td>BankName</td>
<td><jsp:getProperty property="bankname" name="beanUsed"/></td>
</tr>
<tr>
<td>Account</td>
<td><jsp:getProperty property="account" name="beanUsed"/></td>
</tr>
<tr>
<td>SSN</td>
<td><jsp:getProperty property="ssn" name="beanUsed"/></td>
</tr>
</table>

<a href="../html/home.html">HOME </a>
</body>
</html>