<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;}

h2{
			text-align: center;
			text-transform: uppercase;
			border: 10px solid grey;
			padding: 10px 0;
		}

</style>
</head>
<body>

<form action="/MyPracticeCRUDs/UpdateExecuteServletpath" method="post">
<jsp:useBean id="beanUsed" class="com.bean.Bean" scope="session"/>	
<h2>You have registered SUCCESSFULLY !!!</h2>
<table id="customers">
<tr>
<td> FirstName</td>
<td><jsp:getProperty property="firstName" name="beanUsed"/></td>
<td><input type="text" placeholder="enter new value" name="fname"></td>
<td></td>
</tr>
<tr>
<td>lastName</td>
<td> <jsp:getProperty property="lastName" name="beanUsed"/></td>
<td><input type="text" placeholder="enter new value" name="lname"></td>
</tr>
<td></td>
<tr>
<td>MiddleName</td>
<td><jsp:getProperty property="middleName" name="beanUsed"/></td>
<td><input type="text" placeholder="enter new value" name="mname"></td>
</tr>

<tr>
<td>Address</td>
<td><jsp:getProperty property="address" name="beanUsed"/></td>
<td><input type="text" placeholder="enter new value" name="address_"></td>
</tr>
<tr>
<td>City</td>
<td><jsp:getProperty property="city" name="beanUsed"/></td>
<td><input type="text" placeholder="enter new value" name="city_"></td>
</tr>
<tr>
<td>State</td>
<td><jsp:getProperty property="state" name="beanUsed"/></td>
<td><input type="text" placeholder="enter new value" name="state_"></td>
</tr>
<tr>
<td>Country</td>
<td><jsp:getProperty property="country" name="beanUsed"/></td>
<td><input type="text" placeholder="enter new value" name="country_"></td>
</tr>
<tr>
<td>Phone</td>
<td> <jsp:getProperty property="phone" name="beanUsed"/></td>
<td><input type="text" placeholder="enter new value" name="phone_"></td>
</tr>
<tr>
<td>BankName</td>
<td><jsp:getProperty property="bankname" name="beanUsed"/></td>
<td><input type="text" placeholder="enter new value" name="bankname_"></td>
</tr>
<tr>
<td>Account</td>
<td><jsp:getProperty property="account" name="beanUsed"/></td>
<td><input type="text" placeholder="enter new value" name="account_"></td>
</tr>
<tr>
<td>SSN</td>
<td><jsp:getProperty property="ssn" name="beanUsed"/></td>
<td><input type="text" placeholder="enter new value" name="ssn_"></td>
</tr>
</table>
<% 
session.setAttribute("full_path",beanUsed.getFirstName()); 

%>

<button>UPDATE NEW VALUES</button>
</form>
<a href="../html/home.html">HOME </a>
</body>
</html>