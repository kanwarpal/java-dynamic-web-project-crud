package com.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bean.Bean;

public class DAO {

	public static Connection getConnection() throws Exception{
		Connection dbConn = null;
    try{
    //step1:
    Class.forName("com.mysql.jdbc.Driver");
	//step2
    String url = "jdbc:mysql://localhost:3306/crud";
	//step3
	dbConn = DriverManager.getConnection(url, "root", "root");
	}
     catch (SQLException sqle) {
	   sqle.printStackTrace();
	   throw sqle;
	}
         catch(Exception e) {
	   e.printStackTrace();
	   throw e;
	}
return dbConn;
}



//Queries

public static void getAllUsers() throws Exception{

    Connection dbConn = null;
    PreparedStatement pStmt = null;
        ResultSet rs = null;

     try{

	dbConn = getConnection();
	pStmt = dbConn.prepareStatement("SELECT * FROM userInfo");
	rs = pStmt.executeQuery();
	while (rs.next()) {
			System.out.println(rs.getString(1));
			System.out.println(rs.getString(2));
			System.out.println(rs.getString(3));
			System.out.println(rs.getString(4));
			System.out.println(rs.getString(5));
			System.out.println(rs.getString(6));
			System.out.println(rs.getString(7));
			System.out.println(rs.getString(8));
			System.out.println(rs.getString(9));
			System.out.println(rs.getString(10));
			System.out.println(rs.getString(11));
		}
     }catch (SQLException sqle) {
	   sqle.printStackTrace();
	   throw sqle;
	}catch(Exception e) {
	   e.printStackTrace();
	   throw e;
	}
     finally {
	rs.close();
	pStmt.close();
	dbConn.close();
    }
	return;
}

public static void addUser(String firstName, String lastName, String middleName,
			String address, String city, String state, String country, String phone,
			String bankname, String account, String ssn)  throws Exception{
    Connection dbConn = null;
    PreparedStatement pStmt = null;
    int rowsInserted = 0;

     try{
	dbConn = getConnection();
	pStmt = dbConn.prepareStatement("INSERT INTO userInfo VALUES (?, ?,?,?,?,?,?,?,?,?,?)");
	pStmt.setString(1, firstName);
	pStmt.setString(2, lastName);
	pStmt.setString(3, middleName);
	pStmt.setString(4, address);
	pStmt.setString(5, city);
	pStmt.setString(6, state);
	pStmt.setString(7, country);
	pStmt.setString(8, phone);
	pStmt.setString(9, bankname);
	pStmt.setString(10, account);
	pStmt.setString(11, ssn);
	
		rowsInserted = pStmt.executeUpdate();

	if (rowsInserted != 1) {
		throw new Exception("Error in inserting the row");

		}
     }catch (SQLException sqle) {
	     System.out.println(sqle.getErrorCode());
	     System.out.println(sqle.getMessage());
	   sqle.printStackTrace();
	   throw sqle;
	}
         catch(Exception e) {
	   e.printStackTrace();
	   throw e;
	}
     finally {
	pStmt.close();
	dbConn.close();
     }
	return;
}


public static Bean getUser(String firstName) throws SQLException, Exception{
	Bean bean = new Bean();
    Connection dbConn = null;
    PreparedStatement pStmt = null;
        ResultSet rs = null;

   try{
	dbConn = getConnection();
	
	pStmt = dbConn.prepareStatement("SELECT * FROM userInfo WHERE firstName = ?");
	pStmt.setString(1, firstName);
		rs = pStmt.executeQuery();
		
		
	if (rs.next()) {
		System.out.println(rs.getString(1));
		System.out.println(rs.getString(2));
		System.out.println(rs.getString(3));
		System.out.println(rs.getString(4));
		System.out.println(rs.getString(5));
		System.out.println(rs.getString(5));
		System.out.println(rs.getString(6));
		System.out.println(rs.getString(7));
		System.out.println(rs.getString(8));
		System.out.println(rs.getString(9));
		System.out.println(rs.getString(10));
		System.out.println(rs.getString(11));
		
		bean.setFirstName(rs.getString(1));
		bean.setLastName(rs.getString(2));
		bean.setMiddleName(rs.getString(3));
		bean.setAddress(rs.getString(4));
		bean.setCity(rs.getString(5));
		bean.setState(rs.getString(6));
		bean.setCountry(rs.getString(7));
		bean.setPhone(rs.getString(8));
		bean.setBankname(rs.getString(9));
		bean.setAccount(rs.getString(10));
		bean.setSsn(rs.getString(11));
		}
   }catch(SQLException sqe){
	   //log the error
	   sqe.printStackTrace();
	   //throw the exception
	   throw sqe;
   } finally{
	   rs.close();
	   pStmt.close();
	   dbConn.close();
   }

	return bean;
}

public static void deleteUser(String inGroupID) throws Exception{
    Connection dbConn = null;
    PreparedStatement pStmt = null;
    int rowsDeleted = 0;

     try{
	dbConn = getConnection();
	pStmt = dbConn.prepareStatement("DELETE FROM userInfo WHERE firstName = ?");
	pStmt.setString(1, inGroupID);
	
		rowsDeleted = pStmt.executeUpdate();

	if (rowsDeleted != 1) {
		throw new Exception("Error in delete the row");

		}
     }catch (SQLException sqle) {
	   sqle.printStackTrace();
	   throw sqle;
	}
         catch(Exception e) {
	   e.printStackTrace();
	   throw e;
	}
     finally {
	pStmt.close();
	dbConn.close();
     }
	return;
}

public static void updateUser(String firstName, String lastName, String middleName,
		String address, String city, String state, String country, String phone,
		String bankname, String account, String ssn,String key)  throws Exception{
    Connection dbConn = null;
    PreparedStatement pStmt = null;
    int rowsUpdated = 0;

     try{
	dbConn = getConnection();
	pStmt = dbConn.prepareStatement("UPDATE userInfo SET firstName=?, lastName=?, middleName=?, address=?, city=?, state=?, country=?, phone=?, bankname=?, account=?, ssn=? where firstName = ?");

	pStmt.setString(1, firstName);
	pStmt.setString(2, lastName);
	pStmt.setString(3, middleName);
	pStmt.setString(4, address);
	pStmt.setString(5, city);
	pStmt.setString(6, state);
	pStmt.setString(7, country);
	pStmt.setString(8, phone);
	pStmt.setString(9, bankname);
	pStmt.setString(10, account);
	pStmt.setString(11, ssn);
	pStmt.setString(12, key);

	
	
		rowsUpdated = pStmt.executeUpdate();

		System.out.println(rowsUpdated);
	if (rowsUpdated != 1) {
		throw new Exception("Error in updating the row");

		}
     }catch (SQLException sqle) {
	   sqle.printStackTrace();
	   throw sqle;
	}
         catch(Exception e) {
	   e.printStackTrace();
	   throw e;
	}
     finally {
	pStmt.close();
	dbConn.close();
     }
	return;
}


public static void main(String[] arg){
try{

DAO.getAllUsers();
//DAO.addUser(firstName, lastName, middleName, address, city, state, country, phone, bankname, account, ssn);
//DAO.getUser(firstName);	

} catch (Exception e){
	e.printStackTrace();
}
}
}
