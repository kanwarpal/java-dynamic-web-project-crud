package com.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bean.Bean;
import com.sql.DAO;

/**
 * Servlet implementation class ReadServlet
 */
@WebServlet("/ReadServletpath")
public class ReadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String firstName = request.getParameter("firstName");
		Bean bean = new Bean();
		
		try {
			bean = DAO.getUser(firstName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Set bean is: "+
						" \nfirstname: "+ bean.getFirstName()+
						" \nlastname: "+ bean.getLastName()+
						" \nmiddlename: "+ bean.getMiddleName()+
						" \naddress: "+ bean.getAddress()+
						" \ncity: "+ bean.getCity()+
						" \nstate: "+ bean.getState()+
						" \ncountry: "+ bean.getCountry()+
						" \nphone: "+ bean.getPhone()+
						" \nbankname: "+ bean.getBankname()+
						" \naccount: "+ bean.getAccount()+
						" ssn: "+ bean.getSsn()
						);
		
		HttpSession session = request.getSession();
		session.setAttribute("beanUsed", bean);
		
		response.sendRedirect("jsp/read.jsp");
		
		
	}

}
