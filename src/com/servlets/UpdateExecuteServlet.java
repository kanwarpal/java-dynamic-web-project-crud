package com.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sql.DAO;

/**
 * Servlet implementation class UpdateExecuteServlet
 */
@WebServlet("/UpdateExecuteServletpath")
public class UpdateExecuteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String firstName = request.getParameter("fname");
		String lastName = request.getParameter("lname");
		String middleName = request.getParameter("mname");
		
		String address = request.getParameter("address_");
		String city = request.getParameter("city_");
		String state = request.getParameter("state_");
		String country = request.getParameter("country_");
		String phone = request.getParameter("phone_");
		
		String bankname = request.getParameter("bankname_");
		String account = request.getParameter("account_");
		String ssn = request.getParameter("ssn_");
		
		System.out.println("User Entered for create: "+
							"\n firstname: " + firstName +
							"\n lastname: " + lastName +
							"\n middlename: " + middleName +
							"\n address: " + address +
							"\n city: " + city +
							"\n state: " + state +
							"\n country: " + country +
							"\n bankname: " + bankname +
							"\n account: " + account +
							"\n ssn: " + ssn);
		String key = request.getSession().getAttribute("full_path").toString();
		
		try {
			DAO.updateUser(firstName, lastName, middleName, address, city, state, country, phone, bankname, account, ssn,key);
//			DAO.addUser(firstName, lastName, middleName, address, city, state, country, phone, bankname, account, ssn);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.sendRedirect("html/home.html");
		
		
	}

}
